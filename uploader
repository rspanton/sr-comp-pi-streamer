#!/usr/bin/env python
from argparse import ArgumentParser
import boto3
import config
import fcntl
import os
import time
import sys

def find_files(path):
    files = []

    for dirpath, dirnames, filenames in os.walk(path):
        for fname in filenames:
            filepath = os.path.join(dirpath, fname)

            if not os.path.isfile(filepath):
                continue

            fd = os.open(filepath, os.O_RDONLY)
            try:
                fcntl.flock(fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
                files.append( filepath )
            except IOError:
                pass
            os.close(fd)

    return files

parser = ArgumentParser(description="Upload unlocked files to S3")
parser.add_argument("dir", help="Directory to monitor")
parser.add_argument("--prefix", type=str, help="Prefix to add to object keys")
parser.add_argument("-p", "--poll", type=int, help="Number of seconds between polls", default=1)
parser.add_argument("-b", "--bucket", type=str, help="S3 bucket to upload")
parser.add_argument("--dry-run", action="store_true", help="Don't actually do anything with S3, or delete anything")
parser.add_argument("-c", "--config", type=str, help="Config file")
args = parser.parse_args()

s3 = boto3.resource("s3")

if args.config is not None:
    conf = config.get_config(args.config)
else:
    try:
        conf = config.get_config()
    except IOError:
        "Default config file doesn't exist"
        conf = {}

root = os.path.abspath(args.dir)

if args.bucket is not None:
    bucket = s3.Bucket(args.bucket)
elif "s3_bucket" in conf:
    bucket = s3.Bucket(conf["s3_bucket"])
else:
    print >>sys.stderr, "No S3 bucket specified"
    exit(1)

if args.prefix is not None:
    prefix = args.prefix
elif "cam_shortname" in conf:
    prefix = conf["cam_shortname"]
else:
    print >>sys.stderr, "No prefix specified"
    exit(1)

while True:
    for path in find_files(root):

        relpath = os.path.join( prefix, os.path.abspath(path)[len(root)+1:] )

        print "Uploading:", path, "to", relpath
        if not args.dry_run:
            bucket.upload_file(path, relpath)
        else:
            print path, relpath
        print "Removing:", path
        if not args.dry_run:
            os.remove(path)

    time.sleep(args.poll)
