import yaml

def get_config(path="/etc/pi-streamer"):
    with open(path, "r") as f:
        conf = yaml.load(f)
    return conf

